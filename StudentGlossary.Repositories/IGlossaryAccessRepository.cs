﻿using StudentGlossary.Entities;
using StudentGlossary.Repositories.Base;
using System.Collections.Generic;

namespace StudentGlossary.Repositories
{
    public interface IGlossaryAccessRepository : IRepository<GlossaryAccess>
    {
        GlossaryAccess getGlossaryAccessByGlossaryIdAndUserId(int GlossaryId,int UserId);
        IEnumerable<GlossaryAccess> getGlossaryAccessesByUserId(int UserId);
        IEnumerable<GlossaryAccess> getGlossaryAccessesByGlossaryId(int GlossaryId);
    }
}
