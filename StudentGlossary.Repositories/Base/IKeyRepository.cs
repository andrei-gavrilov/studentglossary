﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentGlossary.Repositories.Base
{
    public interface IKeyRepository<T, K> : IRepository<T> where T : class
    {
        T GetSingle(K entityKey);
    }
}
