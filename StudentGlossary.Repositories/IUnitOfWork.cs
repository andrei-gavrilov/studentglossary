﻿using System;
namespace StudentGlossary.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }
        IGlossaryRepository GlossaryRepository { get; }
        ICourseRepository CourseRepository { get; }
        IWordRepository WordRepository { get; }
        IGlossaryAccessRepository GlossaryAccessRepository { get; }
        void Save();
    }
}
