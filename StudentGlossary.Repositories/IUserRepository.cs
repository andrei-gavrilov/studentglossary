﻿using StudentGlossary.Entities;
using StudentGlossary.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudentGlossary.Repositories
{
    public interface IUserRepository : IRepository<User>, IKeyRepository<User, int>
    {
        IEnumerable<User> getUsersByName(string name);
        IEnumerable<User> getUsersByRole(string role);
        User getUserByEmail(string email);
    }
}
