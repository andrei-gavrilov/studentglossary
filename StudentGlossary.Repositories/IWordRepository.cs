﻿using System;
using System.Collections.Generic;
using System.Text;
using StudentGlossary.Entities;
using StudentGlossary.Repositories.Base;

namespace StudentGlossary.Repositories
{
    public interface IWordRepository : IRepository<Word>, IKeyRepository<Word, int>
    {
        IEnumerable<Word> getWordsByTerm(string term);
        IEnumerable<Word> getWordsByDefinition(string definition);
        IEnumerable<Word> getWordsByGlossary(int id);
        IEnumerable<Word> getWordsByUser(int id);
    }
}
