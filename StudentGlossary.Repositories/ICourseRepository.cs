﻿using StudentGlossary.Entities;
using StudentGlossary.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudentGlossary.Repositories
{
    public interface ICourseRepository : IRepository<Course>, IKeyRepository<Course, string>
    {
        IEnumerable<Course> getCoursesByName(string name);
    }
}
