﻿using System;
using System.Collections.Generic;
using System.Text;
using StudentGlossary.Entities;
using StudentGlossary.Repositories.Base;

namespace StudentGlossary.Repositories
{
    public interface IGlossaryRepository : IRepository<Glossary>, IKeyRepository<Glossary, int>
    {
        IEnumerable<Glossary> getGlossariesByName(string name);
        IEnumerable<Glossary> getGlossariesByAuthor(int entityKey);
    }
}
