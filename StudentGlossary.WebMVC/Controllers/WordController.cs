﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentGlossary.Entities;
using StudentGlossary.Repositories;
using StudentGlossary.WebMVC.Models;

namespace StudentGlossary.WebMVC.Controllers
{
    public class WordController : Controller
    {
        IUnitOfWork uof;

        public WordController(IUnitOfWork uof)
        {
            this.uof = uof;
        }
        [Authorize(Roles = "admin")]
        public IActionResult Index()
        {
            List<Word> list = uof.WordRepository.GetAll().ToList();
            return View(list);
        }
        [Authorize(Roles = "admin")]
        public IActionResult Details(int id)
        {
            Word word = uof.WordRepository.GetSingle(id);
            if (word == null)
                return NotFound();
            return View(word);
        }

        [Authorize]
        public IActionResult View(int id)
        {
            Word word = uof.WordRepository.GetSingle(id);
            if (word == null)
            {
                return NotFound();
            }
            WordViewModel wvm = new WordViewModel
            {
                WordId=word.WordId,
                GlossaryId = word.GlossaryId,
                GlossaryName = word.Glossary.Name,
                Term =word.Term,
                UserName=word.User.Name,
                CreatedAt=word.CreatedAt,
                Definition=word.Definition,
                UserId = int.Parse(User.FindFirst(x => x.Type == "UserId").Value)
            };
            var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
            if (wvm.UserId != userid && wvm.GlossaryUserId!= userid)
                return NotFound();
            return View(wvm);
        }

        [Authorize(Roles = "student, teacher, admin")]
        public IActionResult My()
        {
            List<WordViewModel> list = new List<WordViewModel>();
            uof.WordRepository.getWordsByUser(int.Parse(User.FindFirst(x => x.Type == "UserId").Value)).ToList().ForEach(c =>
            {
                WordViewModel word = new WordViewModel
                {
                    WordId = c.WordId,
                    Term = c.Term,
                    Definition = c.Definition,
                    CreatedAt = c.CreatedAt,
                    GlossaryId = c.GlossaryId,
                    GlossaryName = c.Glossary.Name,
                    UserId = c.UserId,
                    UserName = c.User.Name
                };
                list.Add(word);
            });

            return View(list);
        }
        [Authorize(Roles = "student, teacher, admin")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            Word word = uof.WordRepository.GetSingle(id);
            if (word == null)
                return NotFound();
            var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
            if (word.UserId!= userid && word.Glossary.UserId!=userid)
                return NotFound();
            return View(word);
        }
        [Authorize(Roles = "student, teacher, admin")]
        [HttpPost]
        public IActionResult Delete(Word word)
        {
            var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
            word = uof.WordRepository.GetSingle(word.WordId);
            if (word.UserId != userid && word.Glossary.UserId != userid)
                return NotFound();
            uof.WordRepository.Delete(word);
            uof.Save();
            return Redirect("~/Word/My/");
        }


        [Authorize(Roles = "student, teacher, admin")]
        [HttpGet]
        public IActionResult Edit(int id)
        {

            Word word = uof.WordRepository.GetSingle(id);
            if (word == null)
                return NotFound();
            var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
            if (word.UserId != userid && word.Glossary.UserId != userid)
                return NotFound();
            WordViewModel wvm = new WordViewModel();

            wvm.WordId = word.WordId;
            wvm.Term = word.Term;
            wvm.Definition = word.Definition;
            wvm.UserId = word.User.UserId;
            wvm.UserName = word.User.Name;

            return View(wvm);
        }
        [Authorize(Roles = "student, teacher, admin")]
        [HttpPost]
        public IActionResult Edit(WordViewModel wordViewModel)
        {
            if (ModelState.IsValid)
            {
                Word word = uof.WordRepository.GetSingle(wordViewModel.WordId);
                var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
                if (word.UserId != userid && word.Glossary.UserId != userid)
                    return NotFound();
                word.Term = wordViewModel.Term;
                word.Definition = wordViewModel.Definition;

                uof.WordRepository.Edit(word);
                uof.Save();
                return Redirect("~/Word/View/" + word.WordId);
            }
            else
            {
                return View(wordViewModel);
            }
        }
        [Authorize(Roles = "student, teacher, admin")]
        [HttpPost]
        public IActionResult Create(WordViewModel wvm)
        {

            if (ModelState.IsValid)
            {
                Word word = new Word()
                {
                    Term = wvm.Term,
                    Definition = wvm.Definition,
                    GlossaryId = wvm.GlossaryId,
                    UserId = int.Parse(User.FindFirst(x => x.Type == "UserId").Value)
                };
                var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
                if (word.UserId != userid && word.Glossary.UserId != userid)
                    return NotFound();
                uof.WordRepository.Add(word);
                uof.Save();
                return Redirect("~/Word/View/" + word.WordId);
            }
            else
            {
                return View(wvm);
            }
        }
        [Authorize(Roles = "student, teacher, admin")]
        [HttpGet]
        public IActionResult Create(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary==null)
            {
                return NotFound();
            }
            WordViewModel wvm = new WordViewModel
            {
                GlossaryId = glossary.GlossaryId,
                UserId = int.Parse(User.FindFirst(x => x.Type == "UserId").Value)
            };
            var userid = int.Parse(User.FindFirst(x => x.Type == "UserId").Value);
            if (wvm.UserId != userid && glossary.UserId != userid)
                return NotFound();
            return View(wvm);
        }
    }
}