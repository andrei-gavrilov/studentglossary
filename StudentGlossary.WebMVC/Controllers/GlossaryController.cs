﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentGlossary.Entities;
using StudentGlossary.Repositories;
using StudentGlossary.WebMVC.Models;

namespace StudentGlossary.WebMVC.Controllers
{
    public class GlossaryController : Controller
    {
        IUnitOfWork uof;
        public GlossaryController(IUnitOfWork uof)
        {
            this.uof = uof;
        }

        [Authorize(Roles = "admin")]
        public IActionResult Index()
        {
            List<GlossaryListingViewModel> list = new List<GlossaryListingViewModel>();
            uof.GlossaryRepository.GetAll().ToList().ForEach(c =>
            {
                GlossaryListingViewModel glossary = new GlossaryListingViewModel
                {
                    GlossaryId = c.GlossaryId,
                    Name = c.Name,
                    CourseId = c.CourseId,
                    CourseName = c.Course.Name,
                    UserName = c.User.Name
                };
                list.Add(glossary);
            });
            return View(list);
        }



        [HttpGet]
        [Authorize(Roles = "teacher, admin")]
        public IActionResult Accesses(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary==null || glossary.UserId!= int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }

            GlossaryAccessesViewModel gavm = new GlossaryAccessesViewModel();
            gavm.GrantedUsers = new List<UserViewModel>();
            gavm.SearchedUsers = new List<UserViewModel>();

            
            gavm.Glossary = new GlossaryViewModel()
            {
                
                CourseId = glossary.CourseId,
                Description = glossary.Description,
                Name = glossary.Name,
                GlossaryId = glossary.GlossaryId,
                UserId = glossary.UserId,
                Password = glossary.Password
            };
            uof.GlossaryAccessRepository.getGlossaryAccessesByGlossaryId(id).ToList().ForEach(c =>
            {
                UserViewModel user = new UserViewModel
                {
                    UserId=c.UserId,
                    Name=c.User.Name,
                    Role=c.User.Role
                };
                gavm.GrantedUsers.Add(user);
            });
            return View(gavm);
        }



        [HttpGet]
        [Authorize(Roles = "teacher, admin")]
        public IActionResult Revoke(int GlossaryId,int UserId)
        {
            if (UserId == int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }
            GlossaryAccess ga = uof.GlossaryAccessRepository.getGlossaryAccessByGlossaryIdAndUserId(GlossaryId, UserId);
            if (ga == null)
                return NotFound();
            if (ga.Glossary.UserId!=ga.UserId)
            {
                return NotFound();
            }
            GlossaryRevokeViewModel gr = new GlossaryRevokeViewModel()
            {
                GlossaryId = ga.GlossaryId,
                GlossaryName = ga.Glossary.Name,
                UserEmail = ga.User.Email,
                UserId = ga.UserId,
                UserName = ga.User.Name
            };
            return View(gr);
        }
        [HttpPost]
        [Authorize(Roles = "teacher, admin")]
        public IActionResult Revoke(GlossaryRevokeViewModel glossaryRevoke)
        {
            if (glossaryRevoke.UserId==int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }
            if (glossaryRevoke != null)
            {
                int glossaryId = glossaryRevoke.GlossaryId;
                GlossaryAccess ga = uof.GlossaryAccessRepository.getGlossaryAccessByGlossaryIdAndUserId(glossaryRevoke.GlossaryId,glossaryRevoke.UserId);
                if (ga!=null)
                {
                    uof.GlossaryAccessRepository.Delete(ga);
                    uof.Save();
                    return Redirect("~/Glossary/Accesses/" + glossaryId);
                }
                else
                {
                    return View(glossaryRevoke);
                }
            }
            else
            {
                return View(glossaryRevoke);
            }
            
        }


        [HttpPost]
        [Authorize(Roles = "teacher, admin")]
        public IActionResult EditPassword(GlossaryAccessesViewModel glossaryAccess)
        {
            Glossary glossary= uof.GlossaryRepository.GetSingle(glossaryAccess.Glossary.GlossaryId);
            if (glossary == null )
            {
                return NotFound();
            }
            if (glossary.UserId != int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }
            glossary.Password = glossaryAccess.Glossary.Password;
            uof.GlossaryRepository.Edit(glossary);
            uof.Save();
            return Redirect("~/Glossary/Accesses/" + glossaryAccess.Glossary.GlossaryId);
        }

        [HttpPost]
        [Authorize(Roles = "teacher, admin")]
        public IActionResult SearchUsers(GlossaryAccessesViewModel glossaryAccess)
        {
            if (glossaryAccess.Glossary.UserId != int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }
            List<User> users = uof.UserRepository.GetAll().Where(x => x.Name.Contains(glossaryAccess.SearchedString)).ToList();
            glossaryAccess.SearchedUsers = new List<UserViewModel>();
            users.ForEach(c =>
            {
                UserViewModel user = new UserViewModel
                {
                    UserId = c.UserId,
                    Name = c.Name,
                    Role = c.Role
                };
                glossaryAccess.SearchedUsers.Add(user);
            });
            return Redirect("~/Glossary/Accesses/" + glossaryAccess.Glossary.GlossaryId);
        }

        [Authorize(Roles = "admin")]
        public IActionResult Details(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary == null)
                return NotFound();
            return View(glossary);
        }


        [HttpGet]
        [Authorize(Roles = "student, teacher, admin")]
        public IActionResult View(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary == null)
                return NotFound();


            GlossaryAccess ga = uof.GlossaryAccessRepository.getGlossaryAccessByGlossaryIdAndUserId(id,int.Parse(User.FindFirst(x => x.Type == "UserId").Value));
            if (ga == null)
            {
                return Redirect("~/Glossary/PasswordRequest/" + id);
            }

            GlossaryAndWordsViewModel gawvm = new GlossaryAndWordsViewModel
            {
                Glossary = new GlossaryListingViewModel()
            };
            gawvm.Glossary.GlossaryId = glossary.GlossaryId;
            gawvm.Glossary.Name = glossary.Name;
            gawvm.Glossary.CreatedAt = glossary.CreatedAt;
            gawvm.Glossary.Description = glossary.Description;
            gawvm.Glossary.UserName = glossary.User.Name;
            gawvm.Glossary.UserId = glossary.User.UserId;
            gawvm.Words = new List<WordViewModel>();
            uof.WordRepository.getWordsByGlossary(glossary.GlossaryId).ToList().ForEach(c =>
            {
                WordViewModel word = new WordViewModel
                {
                    WordId = c.WordId,
                    Term = c.Term,
                    CreatedAt=c.CreatedAt,
                    Definition = c.Definition,
                    UserId = c.User.UserId,
                    UserName = c.User.Name,
                    CourseId = c.Glossary.CourseId,
                    CourseName = c.Glossary.Course.Name
                };
                gawvm.Words.Add(word);
            });

            return View(gawvm);
        }
        [HttpGet]
        [Authorize(Roles = "student, teacher, admin")]
        public IActionResult PasswordRequest(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary == null)
                return NotFound();
            GlossaryAccess ga = uof.GlossaryAccessRepository.getGlossaryAccessByGlossaryIdAndUserId(id, int.Parse(User.FindFirst(x => x.Type == "UserId").Value));
            if (ga != null)
            {
                return Redirect("~/Glossary/View/" + id);
            }
            PasswordRequestViewModel pr = new PasswordRequestViewModel
            {
                GlossaryId = glossary.GlossaryId,
                GlossaryName = glossary.Name
            };
            return View(pr);
        }
        [HttpPost]
        [Authorize(Roles = "student, teacher, admin")]
        public IActionResult PasswordRequest(PasswordRequestViewModel pr)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(pr.GlossaryId);
            GlossaryAccess ga = uof.GlossaryAccessRepository.getGlossaryAccessByGlossaryIdAndUserId(glossary.GlossaryId,
                int.Parse(User.FindFirst(x => x.Type == "UserId").Value));
            if (ga!=null)
            {
                return Redirect("~/Glossary/View/" + pr.GlossaryId);
            }
            if (ModelState.IsValid)
            {
                if (pr.Password.Equals(glossary.Password))
                {
                    ga = new GlossaryAccess()
                    {
                        GlossaryId = glossary.GlossaryId,
                        UserId = int.Parse(User.FindFirst(x => x.Type == "UserId").Value)
                    };
                    uof.GlossaryAccessRepository.Add(ga);
                    return Redirect("~/Glossary/View/" + pr.GlossaryId);
                }
                else
                {
                    return View(pr.GlossaryId);
                }
            }
            else
            {
                return View(new PasswordRequestViewModel() { Password=pr.Password});
            }
        }

        [Authorize(Roles = "teacher, admin")]
        public IActionResult My()
        {
            List<GlossaryListingViewModel> list = new List<GlossaryListingViewModel>();
            uof.GlossaryRepository.getGlossariesByAuthor(int.Parse(User.FindFirst(x => x.Type == "UserId").Value)).ToList().ForEach(c =>
            {
                GlossaryListingViewModel glossary = new GlossaryListingViewModel
                {
                    GlossaryId = c.GlossaryId,
                    Name = c.Name,
                    CourseId = c.CourseId,
                    CourseName = c.Course.Name,
                    UserName = c.User.Name,
                    CreatedAt = c.CreatedAt
                };
                list.Add(glossary);
            });
            return View(list);
        }

        [Authorize(Roles = "student, teacher, admin")]
        public IActionResult Glossaries()
        {
            List<GlossaryListingViewModel> list = new List<GlossaryListingViewModel>();
            uof.GlossaryRepository.GetAll().ToList().ForEach(c =>
            {
                GlossaryListingViewModel glossary = new GlossaryListingViewModel
                {
                    GlossaryId = c.GlossaryId,
                    Name = c.Name,
                    CourseId = c.CourseId,
                    CourseName = c.Course.Name,
                    UserName = c.User.Name,
                    CreatedAt = c.CreatedAt
                };
                list.Add(glossary);
            });
            return View(list);
        }
        [Authorize(Roles = "student, teacher, admin")]
        public IActionResult Granted()
        {
            List<GlossaryListingViewModel> list = new List<GlossaryListingViewModel>();
            uof.GlossaryAccessRepository.getGlossaryAccessesByUserId(int.Parse(User.FindFirst(x => x.Type == "UserId").Value)).ToList().ForEach(c =>
            {
                GlossaryListingViewModel glossary = new GlossaryListingViewModel
                {
                    GlossaryId = c.GlossaryId,
                    Name = c.Glossary.Name,
                    CourseId = c.Glossary.CourseId,
                    CourseName = c.Glossary.Course.Name,
                    UserName = c.Glossary.User.Name,
                    CreatedAt = c.Glossary.CreatedAt,
                    UserId = c.Glossary.UserId
                };
                list.Add(glossary);
            });
            return View(list);
        }



        [Authorize(Roles = "teacher, admin")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary == null)
                return NotFound();
            if (glossary.UserId != int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }
            GlossaryDeleteViewModel gdvm = new GlossaryDeleteViewModel()
            {
                GlossaryId = glossary.GlossaryId,
                CourseId=glossary.CourseId,
                CourseName=glossary.Course.Name,
                GlossaryName=glossary.Name
            };
            return View(gdvm);
        }
        [Authorize(Roles = "teacher, admin")]
        [HttpPost]
        public IActionResult Delete(GlossaryDeleteViewModel gdvm)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(gdvm.GlossaryId);
            if (glossary != null && glossary.UserId == int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                uof.GlossaryRepository.Delete(glossary);
            }
            return RedirectToAction("My");
        }



        [Authorize(Roles = "teacher, admin")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            Glossary glossary = uof.GlossaryRepository.GetSingle(id);
            if (glossary == null)
                return NotFound();
            if (glossary.UserId != int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
            {
                return NotFound();
            }

            GlossaryCreateViewModel gcvm = new GlossaryCreateViewModel();
            gcvm.Glossary = new GlossaryViewModel();
            gcvm.Glossary.GlossaryId = glossary.GlossaryId;
            gcvm.Glossary.Name = glossary.Name;
            gcvm.Glossary.Description = glossary.Description;
            gcvm.Glossary.Password = glossary.Password;
            gcvm.Glossary.UserId = glossary.UserId;
            gcvm.Glossary.CourseId = glossary.CourseId;

            gcvm.Courses = new List<CourseViewModel>();
            uof.CourseRepository.GetAll().ToList().ForEach(c =>
            {
                CourseViewModel course = new CourseViewModel
                {
                    CourseId = c.CourseId,
                    Name = c.Name,
                    Eap = c.Eap
                };
                gcvm.Courses.Add(course);
            });
            return View(gcvm);
        }
        [Authorize(Roles = "teacher, admin")]
        [HttpPost]
        public IActionResult Edit(GlossaryCreateViewModel glossaryCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                Glossary glossary = uof.GlossaryRepository.GetSingle(glossaryCreateViewModel.Glossary.GlossaryId);
                if (glossary.UserId != int.Parse(User.FindFirst(x => x.Type == "UserId").Value))
                {
                    return NotFound();
                }

                glossary.Name = glossaryCreateViewModel.Glossary.Name;
                glossary.CourseId = glossaryCreateViewModel.Glossary.CourseId;
                glossary.Description = glossaryCreateViewModel.Glossary.Description;

                uof.GlossaryRepository.Edit(glossary);
                uof.Save();
                return Redirect("~/Glossary/View/" + glossary.GlossaryId);
            }
            else
            {
                glossaryCreateViewModel.Courses = new List<CourseViewModel>();
                uof.CourseRepository.GetAll().ToList().ForEach(c =>
                {
                    CourseViewModel glossary = new CourseViewModel
                    {
                        CourseId = c.CourseId,
                        Name = c.Name,
                        Eap = c.Eap
                    };
                    glossaryCreateViewModel.Courses.Add(glossary);
                });
                return View(glossaryCreateViewModel);
            }
        }



        [Authorize(Roles = "teacher, admin")]
        [HttpGet]
        public IActionResult Create()
        {
            GlossaryCreateViewModel gcvm = new GlossaryCreateViewModel();
            gcvm.Glossary = new GlossaryViewModel();
            gcvm.Courses = new List<CourseViewModel>();
            uof.CourseRepository.GetAll().ToList().ForEach(c =>
            {
                CourseViewModel glossary = new CourseViewModel
                {
                    CourseId = c.CourseId,
                    Name = c.Name,
                    Eap = c.Eap
                };
                gcvm.Courses.Add(glossary);
            });
            return View(gcvm);
        }
        [Authorize(Roles = "teacher, admin")]
        [HttpPost]
        public IActionResult Create(GlossaryCreateViewModel glossaryCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                Glossary glossary = new Glossary()
                {
                    Name = glossaryCreateViewModel.Glossary.Name,
                    CourseId = glossaryCreateViewModel.Glossary.CourseId,
                    UserId = int.Parse(User.FindFirst(x => x.Type == "UserId").Value),
                    Description = glossaryCreateViewModel.Glossary.Description,
                    Password= glossaryCreateViewModel.Glossary.Password,
                    CreatedAt = DateTime.Now
                };
                uof.GlossaryRepository.Add(glossary);

                GlossaryAccess ga = new GlossaryAccess()
                {
                    GlossaryId = glossary.GlossaryId,
                    UserId = glossary.UserId
                };
                uof.GlossaryAccessRepository.Add(ga);
                uof.Save();
                return Redirect("~/Glossary/View/"+glossary.GlossaryId);
            }
            else
            {
                glossaryCreateViewModel.Courses = new List<CourseViewModel>();
                uof.CourseRepository.GetAll().ToList().ForEach(c =>
                {
                    CourseViewModel course = new CourseViewModel
                    {
                        CourseId = c.CourseId,
                        Name = c.Name,
                        Eap = c.Eap
                    };
                    glossaryCreateViewModel.Courses.Add(course);
                });
                return View(glossaryCreateViewModel);
            }
        }
    }
}