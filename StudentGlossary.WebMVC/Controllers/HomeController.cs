﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentGlossary.Entities;
using StudentGlossary.Repositories;
using StudentGlossary.WebMVC.Models;
using StudentGlossary.WebMVC.Services;

namespace StudentGlossary.WebMVC.Controllers
{
    public class HomeController : Controller
    {
        /*public IActionResult Index()
        {
            return View();
        }*/


        IUnitOfWork uof;
        public HomeController(IUnitOfWork uof)
        {
            this.uof = uof;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        [Authorize]
        public IActionResult Search(string searchString)
        {
            List<Glossary> glossaries=new List<Glossary>();
            if (!String.IsNullOrEmpty(searchString))
            {
                glossaries = uof.GlossaryRepository.GetAll().Where(s => s.Name.Contains(searchString)).ToList();
            }
            return View(glossaries);

        }
        [Authorize]
        public IActionResult Search(List<Glossary> glossaries)
        {

            return View(glossaries);

        }
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult E()
        {
            return View();
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
