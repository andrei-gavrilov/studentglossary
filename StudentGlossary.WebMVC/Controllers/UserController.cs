﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudentGlossary.Entities;
using StudentGlossary.Repositories;

namespace StudentGlossary.WebMVC.Controllers
{
    public class UserController : Controller
    {
        IUnitOfWork uof;

        public UserController(IUnitOfWork uof)
        {
            this.uof = uof;
        }



        [Authorize(Roles = "admin")]
        public IActionResult Index()
        {
            List<User> list = uof.UserRepository.GetAll().ToList();
            return View(list);
        }



        [Authorize(Roles = "admin")]
        public IActionResult Details(int id)
        {
            User user = uof.UserRepository.GetSingle(id);
            if (user == null)
                return NotFound();
            return View(user);
        }



        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Delete(int id)
        {
            User user = uof.UserRepository.GetSingle(id);
            if (user == null)
                return NotFound();
            return View(user);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Delete(User user)
        {
            uof.UserRepository.Delete(user);
            uof.Save();
            return View();
        }



        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Edit(int id)
        {
            User user = uof.UserRepository.GetSingle(id);
            if (user == null)
                return NotFound();
            return View(user);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Edit(User user)
        {
            uof.UserRepository.Edit(user);
            uof.Save();
            return RedirectToAction("Index");
        }



        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Create(User user)
        {
            uof.UserRepository.Add(user);
            uof.Save();
            return RedirectToAction("Index");
        }
    }
}