﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentGlossary.Entities;
using StudentGlossary.Repositories;

namespace StudentGlossary.WebMVC.Controllers
{
    public class CourseController : Controller
    {
        IUnitOfWork uof;
        public CourseController(IUnitOfWork uof)
        {
            this.uof = uof;
        }
        [Authorize(Roles = "admin")]
        public IActionResult Index()
        {
            List<Course> list = uof.CourseRepository.GetAll().ToList();
            return View(list);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Delete(string id)
        {
            Course course = uof.CourseRepository.GetSingle(id);
            if (course == null)
                return NotFound();
            return View(course);
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Delete(string id, IFormCollection form)
        {
            Course course = uof.CourseRepository.GetSingle(id);
            if (course != null)
            {
                uof.CourseRepository.Delete(course);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Edit(string id)
        {
            Course course = uof.CourseRepository.GetSingle(id);
            if (course == null)
                return NotFound();
            return View(course);
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Edit(Course course)
        {
            uof.CourseRepository.Edit(course);
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "admin")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Create(Course course)
        {
            uof.CourseRepository.Add(course);
            uof.Save();
            return RedirectToAction("Index");
        }
    }
}
