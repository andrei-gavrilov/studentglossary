﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentGlossary.Database;
using StudentGlossary.Repositories;
using StudentGlossary.RepositoriesDB;
using StudentGlossary.RepositoriesDB.Repositories;

namespace StudentGlossary.WebMVC
{
    public class Startup
    {
        private IServiceCollection services;
        public static IConfiguration Configuration;

        public Startup(IConfiguration config)
        {
            Configuration = config;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            this.services = services;

            //services.AddDbContext<GlossaryContext>(options => options.UseSqlServer(Configuration.GetConnectionString("CollegeDatabase")));
            services.AddDbContext<GlossaryContext>(options => options.UseSqlServer(Configuration.GetConnectionString("GlossaryDatabase")));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => //CookieAuthenticationOptions
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });


            services.AddMvc();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ICourseRepository, CourseRepository>();
            services.AddTransient<IWordRepository, WordRepository>();
            services.AddTransient<IGlossaryRepository, GlossaryRepository>();
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            //services.AddSingleton(Configuration);
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                // New code to handle requests like '/Users/1/BuyProduct/2'
                routes.MapRoute(
                    // Name of the new route, we'll need it later to generate URLs in the templates
                    name: "revoke",
                    // Route pattern
                    template: "{controller}/{GlossaryId}/{action}/{UserId}");
            });
        }
    }
}
