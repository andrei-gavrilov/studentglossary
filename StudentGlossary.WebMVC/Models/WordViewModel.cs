﻿using StudentGlossary.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class WordViewModel
    {
        public int WordId { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Term { get; set; }

        [Required]
        [StringLength(2000, MinimumLength = 5)]
        public string Definition { get; set; }

        public DateTime CreatedAt { get; set; }

        public int GlossaryId { get; set; }
        public string GlossaryName { get; set; }
        public int GlossaryUserId { get; set; }

        public int UserId { get; set; }
        public string UserName { get; set; }

        public string CourseId { get; set; }
        public string CourseName { get; set; }
    }
}
