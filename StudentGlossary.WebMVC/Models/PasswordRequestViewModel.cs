﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class PasswordRequestViewModel
    {
        [StringLength(80)]
        [Required]
        public string Password { get; set; }
        public int GlossaryId { get; set; }
        public string GlossaryName { get; set; }
    }
}
