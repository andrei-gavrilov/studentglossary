﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class CourseViewModel
    {
        public string CourseId { get; set; }
        public string Name { get; set; }
        public int Eap { get; set; }
    }
}
