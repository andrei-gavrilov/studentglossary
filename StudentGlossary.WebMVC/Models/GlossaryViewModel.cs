﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryViewModel
    {
        public int GlossaryId { get; set; }

        [Required]
        [StringLength(80, MinimumLength = 3)]
        [Display(Name = "Glossary Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(4000, MinimumLength = 3)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [StringLength(80, MinimumLength = 5)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [Display(Name = "Course")]
        public string CourseId { get; set; }

        public int UserId { get; set; }
    }
}
