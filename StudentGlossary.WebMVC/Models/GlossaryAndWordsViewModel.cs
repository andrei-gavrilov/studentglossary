﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryAndWordsViewModel
    {
        public GlossaryListingViewModel Glossary { get; set; }
        public List<WordViewModel> Words { get; set; }
    }
}
