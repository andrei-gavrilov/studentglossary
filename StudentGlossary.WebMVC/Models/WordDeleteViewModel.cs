﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class WordDeleteViewModel
    {
        public int WordId { get; set; }
        public string Term { get; set; }
        public string Definition { get; set; }
        public int GlossaryId { get; set; }
        public string GlossaryName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}
