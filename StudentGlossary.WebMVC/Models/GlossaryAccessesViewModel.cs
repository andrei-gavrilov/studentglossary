﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryAccessesViewModel
    {
        public GlossaryViewModel Glossary { get; set; }
        public string SearchedString { get; set; }
        public List<UserViewModel> GrantedUsers { get; set; }
        public List<UserViewModel> SearchedUsers { get; set; }
    }
}
