﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class SearchStringViewModel
    {
        [Required]
        [StringLength(80, MinimumLength = 3)]
        public string SearchString { get; set; }
    }
}
