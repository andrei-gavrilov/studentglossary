﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryIndexViewModel
    {
        public IEnumerable<GlossaryViewModel> Glossaries { get; set; }
    }
}
