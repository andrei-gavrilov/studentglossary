﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryCreateViewModel
    {
        public GlossaryViewModel Glossary { get; set; }
        public List<CourseViewModel> Courses { get; set; }
    }
}
