﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryListingViewModel
    {
        public int GlossaryId { get; set; }
        [Display(Name = "Glossary Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Course Code")]
        public string CourseId { get; set; }
        [Display(Name = "Course Name")]
        public string CourseName { get; set; }
        [Display(Name = "Autor Id")]
        public int UserId { get; set; }
        [Display(Name = "Autor")]
        public string UserName { get; set; }
        [Display(Name = "Created at")]
        public DateTime CreatedAt { get; set; }
    }
}
