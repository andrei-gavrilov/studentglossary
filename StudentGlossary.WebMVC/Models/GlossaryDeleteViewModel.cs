﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentGlossary.WebMVC.Models
{
    public class GlossaryDeleteViewModel
    {
        public int GlossaryId { get; set; }
        public string GlossaryName { get; set; }
        public string CourseId { get; set; }
        public string CourseName { get; set; }
    }
}
