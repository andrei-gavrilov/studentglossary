﻿using System;

namespace StudentGlossary.Database
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            using (var context = new GlossaryContext())
            {
                foreach (var item in context.Users)
                {
                    Console.WriteLine(item.UserId+" "+item.Name+" "+item.Role);
                }
            }
            Console.WriteLine("End!");
            Console.ReadLine();
        }
    }
}
