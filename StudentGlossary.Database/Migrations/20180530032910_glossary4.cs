﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace StudentGlossary.Database.Migrations
{
    public partial class glossary4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "sg");

            migrationBuilder.CreateTable(
                name: "Courses",
                schema: "sg",
                columns: table => new
                {
                    CourseId = table.Column<string>(maxLength: 7, nullable: false),
                    Eap = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.CourseId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "sg",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    Email = table.Column<string>(maxLength: 255, nullable: false),
                    Name = table.Column<string>(maxLength: 80, nullable: false),
                    Password = table.Column<string>(maxLength: 300, nullable: false),
                    Role = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Glossaries",
                schema: "sg",
                columns: table => new
                {
                    GlossaryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseId = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    Description = table.Column<string>(maxLength: 4000, nullable: false),
                    Name = table.Column<string>(maxLength: 80, nullable: false),
                    Password = table.Column<string>(maxLength: 80, nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Glossaries", x => x.GlossaryId);
                    table.ForeignKey(
                        name: "FK_Glossaries_Courses_CourseId",
                        column: x => x.CourseId,
                        principalSchema: "sg",
                        principalTable: "Courses",
                        principalColumn: "CourseId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Glossaries_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "sg",
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "GlossaryAccesses",
                schema: "sg",
                columns: table => new
                {
                    GlossaryId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GlossaryAccesses", x => new { x.GlossaryId, x.UserId });
                    table.ForeignKey(
                        name: "FK_GlossaryAccesses_Glossaries_GlossaryId",
                        column: x => x.GlossaryId,
                        principalSchema: "sg",
                        principalTable: "Glossaries",
                        principalColumn: "GlossaryId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_GlossaryAccesses_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "sg",
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Words",
                schema: "sg",
                columns: table => new
                {
                    WordId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    Definition = table.Column<string>(maxLength: 2000, nullable: false),
                    GlossaryId = table.Column<int>(nullable: false),
                    Term = table.Column<string>(maxLength: 50, nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Words", x => x.WordId);
                    table.ForeignKey(
                        name: "FK_Words_Glossaries_GlossaryId",
                        column: x => x.GlossaryId,
                        principalSchema: "sg",
                        principalTable: "Glossaries",
                        principalColumn: "GlossaryId",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Words_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "sg",
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Glossaries_CourseId",
                schema: "sg",
                table: "Glossaries",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Glossaries_UserId",
                schema: "sg",
                table: "Glossaries",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_GlossaryAccesses_UserId",
                schema: "sg",
                table: "GlossaryAccesses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Words_GlossaryId",
                schema: "sg",
                table: "Words",
                column: "GlossaryId");

            migrationBuilder.CreateIndex(
                name: "IX_Words_UserId",
                schema: "sg",
                table: "Words",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GlossaryAccesses",
                schema: "sg");

            migrationBuilder.DropTable(
                name: "Words",
                schema: "sg");

            migrationBuilder.DropTable(
                name: "Glossaries",
                schema: "sg");

            migrationBuilder.DropTable(
                name: "Courses",
                schema: "sg");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "sg");
        }
    }
}
