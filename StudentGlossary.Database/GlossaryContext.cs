﻿using Microsoft.EntityFrameworkCore;
using StudentGlossary.Entities;

namespace StudentGlossary.Database
{
    public class GlossaryContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Glossary> Glossaries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Word> Words { get; set; }
        public DbSet<GlossaryAccess> GlossaryAccesses { get; set; }

        public GlossaryContext(DbContextOptions<GlossaryContext> options) : base(options)
        { }
        public GlossaryContext()
        { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=db_Gavrilov;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(@"Server=VCT\SQLEXPRESS;Database=db_Gavrilov;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(@"Server=VCT.VK;Database=db_Gavrilov;User Id=t154328;Password=t154328;");




            //optionsBuilder.UseSqlServer(@"Server=vct.vk;Database=db_Gavrilov;User Id=t154328;Password=t154328;");
            optionsBuilder.UseSqlServer(@"Server=www.vk.edu.ee;Database=db_Gavrilov;User Id=t154328;Password=t154328;");




            //optionsBuilder.UseSqlServer(Configuration);
            /*if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("StudentGlossary");
                optionsBuilder.UseSqlServer(connectionString);
            }*/
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("sg");

            modelBuilder.Entity<GlossaryAccess>()
            .HasKey(ga => new { ga.GlossaryId, ga.UserId });

            modelBuilder.Entity<User>()
            .Property(b => b.CreatedAt)
            .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<Word>()
            .Property(b => b.CreatedAt)
            .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<Glossary>()
            .Property(b => b.CreatedAt)
            .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<GlossaryAccess>()
            .Property(b => b.CreatedAt)
            .HasDefaultValueSql("getdate()");
        }

    }
}
