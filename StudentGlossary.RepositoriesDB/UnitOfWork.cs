﻿using System;
using StudentGlossary.Database;
using StudentGlossary.Repositories;
using StudentGlossary.RepositoriesDB.Repositories;


namespace StudentGlossary.RepositoriesDB
{
    public class UnitOfWork : IUnitOfWork
    {
        private GlossaryContext context;
        // Add all the repository handles here
        private IUserRepository userRepository = null;
        private ICourseRepository courseRepository = null;
        private IGlossaryRepository glossaryRepository = null;
        private IWordRepository wordRepository = null;
        private IGlossaryAccessRepository glossaryAccessRepository = null;

        public UnitOfWork()
        {
            this.context = new GlossaryContext();
        }
        public UnitOfWork(GlossaryContext context)
        {
            this.context = context;
        }

        public IUserRepository UserRepository
        {
            get
            {
                return userRepository = userRepository ?? new UserRepository();
            }
        }
        public ICourseRepository CourseRepository
        {
            get
            {
                return courseRepository = courseRepository ?? new CourseRepository();
            }
        }
        public IGlossaryRepository GlossaryRepository
        {
            get
            {
                return glossaryRepository = glossaryRepository ?? new GlossaryRepository();
            }
        }
        public IWordRepository WordRepository
        {
            get
            {
                return wordRepository = wordRepository ?? new WordRepository();
            }
        }
        public IGlossaryAccessRepository GlossaryAccessRepository
        {
            get
            {
                return glossaryAccessRepository = glossaryAccessRepository ?? new GlossaryAccessRepository();
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}


//private Dictionary<string, object> repositories;
//public Repository<T> Repository<T>() where T : BaseEntity
//{
//    if (repositories == null)
//    {
//        repositories = new Dictionary<string, object>();
//    }

//    var type = typeof(T).Name;

//    if (!repositories.ContainsKey(type))
//    {
//        var repositoryType = typeof(Repository<>);
//        var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), context);
//        repositories.Add(type, repositoryInstance);
//    }
//    return (Repository<T>)repositories[type];
//}

