﻿using Microsoft.EntityFrameworkCore;
using StudentGlossary.Database;
using StudentGlossary.Entities;
using StudentGlossary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentGlossary.RepositoriesDB.Repositories
{
    public class GlossaryAccessRepository : IGlossaryAccessRepository
    {
        GlossaryContext context;

        public GlossaryAccessRepository()
        {
            context = new GlossaryContext();
        }

        public IEnumerable<GlossaryAccess> GetAll()
        {
            return context.GlossaryAccesses.Include(u => u.User).Include(c => c.Glossary).AsEnumerable();
        }

        public IEnumerable<GlossaryAccess> FindBy(Func<GlossaryAccess, bool> predicate)
        {
            return context.GlossaryAccesses.Where(predicate);
        }
        public GlossaryAccess getGlossaryAccessByGlossaryIdAndUserId(int GlossaryId, int UserId)
        {
            return context.GlossaryAccesses.Include(u => u.User).Include(c => c.Glossary).Where(x => (x.GlossaryId == GlossaryId) && (x.UserId==UserId)).FirstOrDefault();
        }
        public IEnumerable<GlossaryAccess> getGlossaryAccessesByUserId(int UserId)
        {
            return context.GlossaryAccesses.Include(c => c.Glossary).ThenInclude(c => c.Course).Include(c => c.Glossary).ThenInclude(c => c.User).Include(c => c.User).Where(x =>x.UserId == UserId).AsEnumerable();
        }
        public IEnumerable<GlossaryAccess> getGlossaryAccessesByGlossaryId(int GlossaryId)
        {
            return context.GlossaryAccesses.Include(u => u.User).Include(c => c.Glossary).Where(x => x.GlossaryId == GlossaryId).AsEnumerable();
        }
        public void Add(GlossaryAccess entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.GlossaryAccesses.Add(entity);
            context.SaveChanges();
        }

        public void Delete(GlossaryAccess entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.GlossaryAccesses.Remove(entity);
            context.SaveChanges();
        }



        public void Edit(GlossaryAccess entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            GlossaryAccess originalEntity =getGlossaryAccessByGlossaryIdAndUserId(entity.GlossaryId,entity.UserId);
            if (originalEntity == null)
            {
                throw new ArgumentNullException("originalEntity");
            }
            context.Entry(originalEntity).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
