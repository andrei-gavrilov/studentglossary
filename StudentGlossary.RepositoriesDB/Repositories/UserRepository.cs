﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentGlossary.Repositories;
using StudentGlossary.Database;
using StudentGlossary.Entities;
using Microsoft.EntityFrameworkCore;

namespace StudentGlossary.RepositoriesDB.Repositories
{
    public class UserRepository : IUserRepository
    {
        GlossaryContext context;

        public UserRepository()
        {
            context = new GlossaryContext();
        }

        public IEnumerable<User> GetAll()
        {
            return context.Users.Include(g => g.Glossaries).Include(w => w.Words).AsQueryable();
        }

        public User GetSingle(int entityKey)
        {
            return context.Users.Find(entityKey);
        }

        public IEnumerable<User> FindBy(Func<User, bool> predicate)
        {
            return context.Users.Where(predicate);
        }

        public IEnumerable<User> getUsersByName(string name)
        {
            return context.Users
                .Where(x => x.Name.ToString().ToLower().Contains(name.ToLower()));
        }
        public IEnumerable<User> getUsersByRole(string name)
        {
            return context.Users
                .Where(x => x.Role == name);
        }
        public User getUserByEmail(string email)
        {
            return context.Users.Include(x=>x.Glossaries).Include(x => x.GlossaryAccesses).Include(x => x.Words).Where(x => x.Email.Equals(email, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        public void Add(User entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Users.Add(entity);
            context.SaveChanges();
        }

        public void Delete(User entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Users.Remove(entity);
            context.SaveChanges();
        }



        public void Edit(User entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            User originalEntity = context.Users.Find(entity.UserId);
            if (originalEntity == null)
            {
                throw new ArgumentNullException("originalEntity");
            }
            context.Entry(originalEntity).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
