﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentGlossary.Repositories;
using StudentGlossary.Database;
using StudentGlossary.Entities;

namespace StudentGlossary.RepositoriesDB.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        GlossaryContext context;

        public CourseRepository()
        {
            context = new GlossaryContext();
        }

        public IEnumerable<Course> GetAll()
        {
            return context.Courses.AsQueryable();
        }
        
        public Course GetSingle(string entityKey)
        {
            return context.Courses.Find(entityKey);
        }

        public IEnumerable<Course> FindBy(Func<Course, bool> predicate)
        {
            return GetAll().Where(predicate);
        }

        public IEnumerable<Course> getCoursesByName(string name)
        {
            return context.Courses
                .Where(x => x.Name.ToString().ToLower().Contains(name.ToLower()));
        }


        public void Add(Course entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Courses.Add(entity);
            context.SaveChanges();
        }

        public void Delete(Course entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Courses.Remove(entity);
            context.SaveChanges();
        }



        public void Edit(Course entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            Course originalEntity = context.Courses.Find(entity.CourseId);
            if (originalEntity == null)
            {
                throw new ArgumentNullException("originalEntity");
            }
            context.Entry(originalEntity).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
