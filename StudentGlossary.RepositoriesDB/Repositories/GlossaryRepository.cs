﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentGlossary.Repositories;
using StudentGlossary.Database;
using StudentGlossary.Entities;
using Microsoft.EntityFrameworkCore;

namespace StudentGlossary.RepositoriesDB.Repositories
{
    public class GlossaryRepository : IGlossaryRepository
    {
        GlossaryContext context;

        public GlossaryRepository()
        {
            context = new GlossaryContext();
        }

        public IEnumerable<Glossary> GetAll()
        {
            return context.Glossaries.Include(u => u.User).Include(c => c.Course).AsQueryable();
        }

        public Glossary GetSingle(int entityKey)
        {
            return context.Glossaries.Include(u => u.User).Include(w => w.Words).Include(w=>w.GlossaryAccesses).Where(x => x.GlossaryId == entityKey).FirstOrDefault();
        }

        public IEnumerable<Glossary> FindBy(Func<Glossary, bool> predicate)
        {
            return context.Glossaries.Where(predicate);
        }

        public IEnumerable<Glossary> getGlossariesByName(string name)
        {
            return context.Glossaries
                .Where(x => x.Name.ToString().ToLower().Contains(name.ToLower()))
                .ToList();
        }
        public IEnumerable<Glossary> getGlossariesByAuthor(int entityKey)
        {
            return context.Glossaries
                .Where(x => x.UserId == entityKey).Include(u => u.User).Include(c => c.Course)
                .ToList();
        }

        public void Add(Glossary entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Glossaries.Add(entity);
            context.SaveChanges();
        }

        public void Delete(Glossary entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            foreach (GlossaryAccess item in entity.GlossaryAccesses)
            {
                context.GlossaryAccesses.Remove(item);
            }
            context.Glossaries.Remove(entity);
            context.SaveChanges();
        }



        public void Edit(Glossary entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            
            Glossary originalEntity = context.Glossaries.Find(entity.GlossaryId);
            if (originalEntity == null)
            {
                throw new ArgumentNullException("originalEntity");
            }
            context.Entry(originalEntity).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
