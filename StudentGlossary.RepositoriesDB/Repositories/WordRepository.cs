﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentGlossary.Repositories;
using StudentGlossary.Database;
using StudentGlossary.Entities;
using Microsoft.EntityFrameworkCore;

namespace StudentGlossary.RepositoriesDB.Repositories
{
    public class WordRepository : IWordRepository
    {
        GlossaryContext context;

        public WordRepository()
        {
            context = new GlossaryContext();
        }

        public IEnumerable<Word> GetAll()
        {
            return context.Words.AsQueryable();
        }

        public Word GetSingle(int entityKey)
        {
            return context.Words.Include(u => u.User).Include(g => g.Glossary).ThenInclude(u=>u.User).Where(w => w.WordId == entityKey).SingleOrDefault();
        }

        public IEnumerable<Word> FindBy(Func<Word, bool> predicate)
        {
            return context.Words.Where(predicate);
        }

        public IEnumerable<Word> getWordsByTerm(string term)
        {
            return context.Words
                .Where(x => x.Term.ToString().ToLower().Contains(term.ToLower()));
        }
        public IEnumerable<Word> getWordsByDefinition(string definition)
        {
            return context.Words
                .Where(x => x.Definition.ToString().ToLower().Contains(definition.ToLower()));
        }
        public IEnumerable<Word> getWordsByGlossary(int id)
        {
            return context.Words.Include(u => u.User).Include(g => g.Glossary).ThenInclude(c => c.Course).Where(x => x.GlossaryId ==id).AsEnumerable();
        }
        public IEnumerable<Word> getWordsByUser(int id)
        {
            return context.Words.Include(g => g.Glossary).Include(u => u.User).Where(x => x.UserId == id).AsEnumerable();
        }

        public void Add(Word entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Words.Add(entity);
            context.SaveChanges();
        }

        public void Delete(Word entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Words.Remove(entity);
            context.SaveChanges();
        }

        public void Edit(Word entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            Word originalEntity = context.Words.Find(entity.WordId);
            if (originalEntity == null)
            {
                throw new ArgumentNullException("originalEntity");
            }
            context.Entry(originalEntity).CurrentValues.SetValues(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
