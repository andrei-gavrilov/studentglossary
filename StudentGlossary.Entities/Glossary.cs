﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StudentGlossary.Entities
{
    public class Glossary
    {
        public int GlossaryId { get; set; }

        [Required]
        [StringLength(80)]
        public string Name { get; set; }

        [Required]
        [StringLength(4000)]
        public string Description { get; set; }
        
        [StringLength(80)]
        public string Password { get; set; }

        public DateTime CreatedAt { get; set; }

        //many to one
        public string CourseId { get; set; }
        public Course Course { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

        //one-to-many
        public ICollection<Word> Words { get; set; }
        public ICollection<GlossaryAccess> GlossaryAccesses { get; set; }
    }
}
