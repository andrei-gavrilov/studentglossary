﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StudentGlossary.Entities
{
    public class User
    {
        public int UserId { get; set; }

        [Required]
        [StringLength(80)]
        public string Name { get; set; }

        [Required]
        [StringLength(300)]
        public string Password { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(10)]
        public string Role { get; set; }

        public DateTime CreatedAt { get; set; }


        //one-to-many
        public ICollection<Word> Words { get; set; }
        public ICollection<Glossary> Glossaries { get; set; }
        public ICollection<GlossaryAccess> GlossaryAccesses { get; set; }
    }
}
