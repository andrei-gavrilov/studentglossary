﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentGlossary.Entities
{
    public class GlossaryAccess
    {
        //many to one
        public int UserId { get; set; }
        public User User { get; set; }
        //many to one
        public int GlossaryId { get; set; }
        public Glossary Glossary { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
