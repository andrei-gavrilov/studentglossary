﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StudentGlossary.Entities
{
    public class Course
    {
        [StringLength(7)]
        public string CourseId { get; set; }

        [Required]
        public string Name { get; set; }

        public int Eap { get; set; }

        //one-to-many
        public ICollection<Glossary> Glossaries { get; set; }
    }
}
