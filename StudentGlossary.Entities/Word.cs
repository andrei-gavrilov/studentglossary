﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StudentGlossary.Entities
{
    public class Word
    {
        public int WordId { get; set; }

        [Required]
        [StringLength(50)]
        public string Term { get; set; }

        [Required]
        [StringLength(2000)]
        public string Definition { get; set; }

        public DateTime CreatedAt { get; set; }

        //many to one
        public int UserId { get; set; }
        public User User { get; set; }
        public int GlossaryId { get; set; }
        public Glossary Glossary { get; set; }
    }
}
